<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <title>StephanePlazaImmo</title>
    <meta name="description" content="<?php echo $description; ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
		integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>

<body>
<nav id="navbar-primary" class="navbar" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-primary-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="navbar-primary-collapse">
            <ul class="nav navbar-nav">
            <li class="active fas fa-home"><a class="menu" href="index.php">Accueil</a></li>
            <li class="fas fa-shopping-cart"><a class="menu" href="details.php">Acheter</a></li>
            <li><img id="logo-navbar-middle" src="images/logo.png" width="200" alt="logo"></li>
            <li class="fas fa-handshake"><a class="menu" href="#">Vendre</a></li>
            <li class="fas fa-warehouse"><a class="menu" href="#">Nos agences</a></li>
            </ul>
        </div>
    </div>
</nav>

