<?php include_once('_inc/header.php')?>
<div class="cont">
  <div class="titre">
    <h1>PROJET IMMOBILIER, PROJET DE VIE ?</h1>
    <p class="lead">ET SI ON PARLAIT DE VOUS ?</p>
  </div>

  <div class="lightgallery">
    <ul id="lightgallery">
      <?php for($i=1;$i<9;$i++): ?>
        <li data-responsive="images/<?php echo $i?>.jpg" data-src="images/<?php echo $i?>.jpg"
      data-sub-html=" <a href='details.php' class='btn'>EXPLORER</a>" data-pinterest-text="" data-tweet-text="">
        <a href="details.php">
          <img class="img-responsive lazyload" src="images/<?php echo $i?>.jpg">
        </a>
      </li>
    <?php endfor ?>
    </ul>
  </div>
</div>

<?php include_once('_inc/footer.php')?>

