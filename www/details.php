<?php include_once('_inc/header.php')?>
<div class="container-act">



<ul id="imageGallery">
    <?php for($i=1;$i<9;$i++): ?>
        <li data-thumb="img/thumb/cS-1.jpg" data-src="images/<?php echo $i?>.jpg">
            <img class="img-responsive lazyload" src="images/<?php echo $i?>.jpg">
        </li>
    <?php endfor ?>
</ul>


    <div class="col-md-4">
            <article class="introduction">
                <h2 itemprop="name">APPARTEMENT, CENTRE DE PARIS</h2>

                <div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating%22%3E">
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star"></span>
                    <meta itemprop="ratingValue" content="4" />
                    <meta itemprop="bestRating" content="5" />
                    Selon l'avis de <span itemprop="ratingCount">25</span> notes d'anciens locataires <br>
                </div>
                <div itemprop="offers" itemscope itemtype="http://schema.org/Offer%22%3E">
                    <span itemprop="price" content="210.000">210 000</span><span itemprop="priceCurrency" content="EUR">€</span><br/>
                    <link itemprop="availability" href="http://schema.org/InStock" />Disponible dès maintenant !
                </div>
                    <div id="infos_bien">
                        <div class="info">
                            <i class="far fa-handshake"></i>
                            <p> A vendre </p>
                        </div>
                        <div class="info">
                            <i class="far fa-building"></i>
                            <p> Appartement </p>
                        </div>
                        <div class="info">
                            <i class="fas fa-bed"></i>
                            <p> 3 Chambres </p>
                        </div>
                    </div>
                <p itemprop="description">
                    Plein centre de Paris, T3 orienté sud. Situé au 4eme et dernier étage d'une copropriété accueillante. A proximité de la seine, des transports en commun et de nombreux commerces !
                </p><br/>
                <div >
                </div>
            </article>
        </div>
    </div>
</div>





<?php include_once('_inc/footer.php')?>