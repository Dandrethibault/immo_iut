//@prepros-prepend ../../node_modules/jquery/dist/jquery.js
//@prepros-prepend ../../node_modules/bootstrap/dist/js/bootstrap.min.js
//@prepros-prepend ../../node_modules/owl.carousel/dist/owl.carousel.js
//@prepros-prepend ../../node_modules/lightgallery/dist/js/lightgallery.js
//@prepros-prepend ../../node_modules/lightgallery/modules/lg-thumbnail.js
//@prepros-prepend ../../node_modules/lazysizes/lazysizes.min.js
//@prepros-prepend ../../node_modules/lightslider/dist/js/lightslider.min.js
//@prepros-prepend ../../node_modules/tingle.js/dist/tingle.min.js

$(document).ready(function () {

    console.log("oui");

    $('#imageGallery').lightSlider({
        nav: true,
        controls: true,
        adaptiveHeight: true,
        item:1,
        loop:true,
        thumbItem:9,
        slideMargin:0,
        enableDrag: false,
        currentPagerPosition:'left',
        onSliderLoad: function(el) {
            el.lightGallery({
                selector: '#imageGallery .lslide'
            });
        }   
    });  


    $('#owl-demo').owlCarousel({
        loop:true,
        margin:10,
        autoHeight:true,
        nav:false,
        items: 1,
        })

    //lightgallery(document.getElementById('lightgalleryD'));
    $('#lightgalleryD').lightGallery();

var modal = new tingle.modal({
    footer: true,
    stickyFooter: false,
    closeMethods: ['overlay', 'button', 'escape'],
    closeLabel: "Close",
    cssClass: ['custom-class-1', 'custom-class-2'],
    onOpen: function() {
        console.log('modal open');
    },
    onClose: function() {
        console.log('modal closed');
    },
    beforeClose: function() {
        // here's goes some logic
        // e.g. save content before closing the modal
        return true; // close the modal
        return false; // nothing happens
    }
});


var mail = document.getElementById("email");
var email = mail.value;
if((email.includes('@') == true) && (email.includes('.') == true))
{
    modal.setContent('<h1>Vous êtes bien inscrits à notre Newsletter</h1> <br/> <h2>Nous vous avons envoyé un mail de confirmation à votre adresse '+email+'</h2>');
    modal.open();
}
else if(email !== "")
{
    modal.setContent('<h1>L\'adresse mail que vous avez entré n\'est pas valide</h1>');
    modal.open();
}


});

