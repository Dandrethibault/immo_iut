# Immo_IUT

#Stephan Plaza Immo

Affichez sur une grille plusieurs biens immobiliers en vente. Au click chargez la page sur un bien (n'en faites qu'un), avec un slider photo consultable en plein écran. 

Utilisez FontAwesome pour illustrer les caractéristiques spécifiques de chaque bien (prix, maison/appartement, surface).

---

Vous devrez travailler en groupe à l'aide de `GIT` et `npm`. Pensez bien à l'organisation du projet et à communiquer.
Pour rappel, certains fichiers sont dispensables, il faut qu'ils soient ignorés (exemple : node_modules).

N'utilisez ni CDN, ni téléchargement manuel, à l'exception des images.

Chaque projet doit avoir un pied de page avec un zone d'inscription à une newsletter. Affichez un message de confirmation lors de l'inscription en utilisant [tingle.js](https://robinparisi.github.io/tingle/). 

Enfin, une série de plugins sont à intégrer **obligatoirement** pour ce projet : 

##Plugins
- [Lazysizes](https://afarkas.github.io/lazysizes/index.html)
- [LightGallery (version Jquery)](http://sachinchoolur.github.io/lightGallery/)
- [Owl Carousel 2](https://github.com/OwlCarousel2/OwlCarousel2)
- [Fontawesome@4](https://fontawesome.com/v4.7.0/get-started/)
